# CefSharp Build With Mp3 And Mp4 Support
#### 用法
clone项目到本地后,在vs中或Nuget.config中添加一个本地文件源即可

#### 介绍
support mp3/mp4

updated at: 2021-03-29，add 88.0.4324.182


```86.0.4240.198```
![](https://images.gitee.com/uploads/images/2021/0102/220959_0d4e1c27_61974.png "image-8e9edc64a8dc447ba93cfd34f501c208.png")


```88.0.4324.182```
![](https://images.gitee.com/uploads/images/2021/0329/095414_ce75b52b_61974.png "88.0.4324.182.png")
